import React, { Component } from 'react';

class Controls extends Component {
  constructor(props) {
    super(props);
    // this.enableControls = () => {
    //   const selectedTask = this.props.selectedTask
    //   if (selectedTask) {
    //     if (selectedTask.stage === 0) {
    //       this.setState({controlsDisable: { back: true, forward: false}})
    //     } else if (selectedTask.stage === 3) {
    //       this.setState({controlsDisable: { back: false, forward: true}})
    //     } else {
    //       this.setState({
    //         controlsDisable: {
    //           back: false,
    //           forward: false
    //         }
    //       })
    //     }
    //   } else {
    //     this.setState({
    //       controlsDisable: {
    //         back: true,
    //         forward: true
    //       }
    //     })
    //   }
    // }
  }
  // componentDidUpdate(prevProps) {
  //   if (prevProps.selectedTask !== this.props.selectedTask)
  //     this.enableControls()
  // }
  render() {
    const { selectedTask, move, controlsDisable } = this.props
    let taskName = ""
    if (selectedTask) {
      taskName = selectedTask.name
    }
    return (
      <div style={{ padding: '1rem', background: '#D6F3FF' }}>
        <h1>Controls</h1>
        <div style={{ display: 'flex', marginTop: '1rem' }}>
          <input
            readOnly
            placeholder="Selected task name"
            style={{ fontSize: '1rem' }}
            data-testid="selected-task-field"
            value={taskName}
          />
          <button
            style={{ marginLeft: '1rem' }}
            disabled={controlsDisable.back}
            data-testid="move-back-btn"
            onClick={() => move(selectedTask, "back")}
          >
            Move back
          </button>
          <button
            style={{ marginLeft: '1rem' }}
            disabled={controlsDisable.forward}
            data-testid="move-forward-btn"
            onClick={() => move(selectedTask, "forward")}
          >
            Move forward
          </button>
        </div>
      </div>
    )
  }
}

export default Controls;
