import React, { Component } from 'react';
import './App.css';

import Controls from './components/Controls';
import Board from './components/Board';

const NUM_STAGES = 4;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [
          { name: 'task 0', stage: 0 },
          { name: 'task 1', stage: 0 },
          { name: 'task 2', stage: 0 },
          { name: 'task 3', stage: 0 },
          { name: 'task 4', stage: 1 },
          { name: 'task 5', stage: 1 },
          { name: 'task 6', stage: 1 },
          { name: 'task 7', stage: 2 },
          { name: 'task 8', stage: 2 },
          { name: 'task 9', stage: 3 },
      ],
      selectedTask: null,
      controlsDisable: {
        back: true,
        forward: true
      }
    };
    this.stagesNames = ['Backlog', 'To Do', 'Ongoing', 'Done'];
    this.enableControls = (selectedTask) => {
      if (selectedTask.stage === 0) {
        this.setState({controlsDisable: { back: true, forward: false }})
      } else if (selectedTask.stage === NUM_STAGES - 1) {
        this.setState({controlsDisable: { back: false, forward: true }})
      } else {
        this.setState({controlsDisable: { back: false, forward: false }})
      }
    }
    this.toggleSelect = (taskName) => {
      let selectedTask = this.state.tasks.find(task => task.name === taskName)
      this.enableControls(selectedTask)
      this.setState({selectedTask: {name: selectedTask.name, stage: selectedTask.stage}})
    }
    this.move = (selectedTask, direction) => {
      if (direction === "back" && selectedTask.stage > 0) {
        selectedTask.stage -=1
      } else if (direction === "forward" && selectedTask.stage < NUM_STAGES - 1) {
        selectedTask.stage +=1
      }
      // update the selected task
      this.setState({
        selectedTask: {
          name: selectedTask.name,
          stage: selectedTask.stage,
        }
      })
      // update the tasks
      this.setState((state, props) => {
        state.tasks.map((task, index) => {
          if (task.name === selectedTask.name) {
            task.stage = selectedTask.stage
          }
          return task
        })
        return state
      })
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.selectedTask !== this.state.selectedTask)
      this.enableControls(this.state.selectedTask)
  }

  render() {
    const { tasks, selectedTask, controlsDisable } = this.state;

    let stagesTasks = [];
    for (let i = 0; i < NUM_STAGES; ++i) {
      stagesTasks.push([]);
    }
    for (let task of tasks) {
      const stageId = task.stage;
      stagesTasks[stageId].push(task);
    }

    return (
      <div className="App">
        <Controls selectedTask={selectedTask} move={this.move} controlsDisable={controlsDisable} />
        <Board
          stagesTasks={stagesTasks}
          stagesNames={this.stagesNames}
          toggleSelect={this.toggleSelect}
        />
      </div>
    );
  }
}

export default App;
